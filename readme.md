This is [SmartCopilot](https://sky4crew.com/smartcopilot/) config file for [Nimbus Simulation Studios](https://www.facebook.com/nimbussimulationstudios/)
UH-1 Huey helicopter addon for X-Plane simulator.

Credits goes to Santiago from Nimbus simulations, who provided me with datarefs I couldn't find myself.
Without his help, we wouldn't have weight and ballance datarefs available.

!!!!!!!!!!!!!!!!!!!!!!!!!!  ATTENTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  All switches, knobs in cockpit should work. However, the throttle is problematic. Both pilots needs to throthle up or down !!!
!!!  to set it to desired positon!                                                                                              !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Visibility of doors, guns and crew should work as well as wight and ballance.
Maintenance datarefs are not shared, as it did strange things.
